#pragma once

#include"Functions.h"
#include"event.h"
#include<ctime>
#include<vector>
#include<fstream>
#include<sstream>

using namespace std;

class Game
{
public:
    Game();
    virtual ~Game();

    //Operators

    //Functions
    void initGame();
    void mainMenu();
    void creatNewCharacter();
    void levelUpCharacter();
    void saveCharacters();
    void loadCharacters();
    void Travel();

    //Accessors
   inline bool getPlaying() const { return this->playing; }

    //Modifiers

private:
    int choice;
    bool playing;

    //Character related
    int activeCharacter;
    vector<Character> characters;
    string fileName;
};
