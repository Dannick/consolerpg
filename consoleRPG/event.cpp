#include "event.h"

Event::Event()
{
    this->nrOfEvents = 2;
}

Event::~Event()
{

}

void Event::generateEvent(Character &character)
{
    int i = rand() % this->nrOfEvents;

    switch (i)
    {
    case 0:
        //Enemy encounter
        enemyEncounter(character);
        break;
    case 1:
        //Puzzle
        puzzleEncounter(character);
        break;
    case 2:
        //Trap
        break;

    default:
        break;
    }
}

//Different events
void Event::enemyEncounter(Character &character)
{
    //while()
}

void Event::puzzleEncounter(Character &character)
{
    bool completed = false;
    int userAns = 0;
    int chances = 3;
    int gainEXP = (chances * character.getLevel() * (rand()%10+1));

    Puzzle puzzle("Puzzles/1.txt");

    while(!completed && chances > 0)
    {
        cout << "Chances left: " << chances << "\n";
        chances--;
        cout << puzzle.getAsString() << "\n";

        cout << "Your answer: ";
        cin >> userAns;
        cout << "\n";

        while (cin.fail())
        {
            cout << "Faulty input!" << "\n";
            cin.clear();
            cin.ignore(100, '\n');

            cout << "\nYour answer: ";
            cin >> userAns;
        }

        cin.ignore(100, '\n');
        cout << "\n";

        if(puzzle.getCorrectAns() == userAns)
        {
            completed = true;
            cout << "CONGRATZ YOU SUCCEDED! \n\n";
        }
        else
        {
            cout << "Wrong answer! Try again!\n";
        }
    }

    if(completed == true)
    {
        //Give user EXP etc and continue
        character.gainExp(gainEXP);
        cout << "You gained:" << gainEXP << " EXP!" << "\n\n";
    }
    else
    {
        cout << "YOU FAILED! \n\n";
    }
}
