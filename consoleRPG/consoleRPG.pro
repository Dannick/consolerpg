TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Game.cpp \
        armor.cpp \
        character.cpp \
        enemy.cpp \
        event.cpp \
        inventory.cpp \
        item.cpp \
        main.cpp \
        puzzle.cpp \
        weapon.cpp

HEADERS += \
    Functions.h \
    Game.h \
    armor.h \
    character.h \
    enemy.h \
    event.h \
    inventory.h \
    item.h \
    puzzle.h \
    weapon.h
