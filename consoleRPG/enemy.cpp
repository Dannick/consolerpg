#include "enemy.h"

Enemy::Enemy(int level)
{
    this->level = level;
    this->hpMax = level * 10;
    this->hp = this->hpMax;
    this->damageMin = this->level * 4;
    this->damageMax = this->level * 5;
    this->dropChance = rand() % 100;
    this->defence = rand() % 100;
    this->accuracy = rand() % 100;
}

Enemy::~Enemy()
{

}

string Enemy:: getAssString()const
{
    return "Level: " + to_string(this->level) + "\n" +
            "Hp: " + to_string(this->hpMax) + " - " + to_string(this->hp) + "\n" +
            "Defence: " + to_string(this->defence) + "\n" +
            "Damage: " + to_string(this->damageMin) + " - " + to_string(this->damageMax) + "\n" +
            "Accuracy: " + to_string(this->accuracy) + "\n" +
            "Drop chance: " + to_string(this->dropChance) + "\n";
}
