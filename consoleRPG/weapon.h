#pragma once
#include "item.h"

using namespace std;

class Weapon :
        public Item
{
private:
    int damageMin;
    int damageMax;

public:
    Weapon(int damageMin = 0, int damageMax = 0,
           string name = "NONE", int level = 0, int buyValue = 0,
           int sellValue = 0, int rarity = 0);
    virtual ~Weapon();


    //Pure virtual
    virtual Weapon* clone()const;

    //Functions
    string toString();

};
